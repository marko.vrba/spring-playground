package com.mvrbancic.learningspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningSpringProjetcApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningSpringProjetcApplication.class, args);
	}

}
