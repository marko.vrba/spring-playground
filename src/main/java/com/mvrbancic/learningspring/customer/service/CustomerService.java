package com.mvrbancic.learningspring.customer.service;

import com.mvrbancic.learningspring.customer.entity.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    Customer create(Customer customer);
    Customer update(Long id, Customer customer);
    List<Customer> findAll();
    Optional<Customer> findOne(Long id);
    void delete(Long id);
}
