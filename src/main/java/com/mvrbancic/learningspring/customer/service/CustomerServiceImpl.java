package com.mvrbancic.learningspring.customer.service;

import com.mvrbancic.learningspring.config.NotFoundException;
import com.mvrbancic.learningspring.customer.entity.Customer;
import com.mvrbancic.learningspring.customer.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Long id, Customer customer) {
        Customer existingCustomer = customerRepository.findById(id).orElse(null);
        if (existingCustomer != null) {
            customer.setId(existingCustomer.getId());
            return customerRepository.save(customer);
        } else {
            throw new NotFoundException("Customer not found with id: " + id);
        }
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Optional<Customer> findOne(Long id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (!customer.isPresent()) {
            throw new NotFoundException("Customer not found with id: " + id);
        }
        return customer;
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }
}
